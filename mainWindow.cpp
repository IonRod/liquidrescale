#include <QDebug>
#include "mainWindow.h"
#include "ui_mainWindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    isLoaded = false;
    ui->lblCanvas->installEventFilter(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

//--------------------------

void MainWindow::drawFrame()
{
    if(isLoaded)
    {
        QPainter painter(ui->lblCanvas);
        QImage temp;
        resampler.getScaledQImage(temp);
        //qDebug()<<"getScaledQImage";
        painter.drawImage(QRect(0, 0, temp.width(), temp.height()), temp);
        //qDebug()<<"finished drawFrame";
    }
    else
    {
        QPainter painter(ui->lblCanvas);
        QRect rect = ui->lblCanvas->rect();
        painter.fillRect(rect, QColor(0,0,0));

    }
}

void MainWindow::loadImage()
{
    QString fileName = QFileDialog::getOpenFileName(this, "Load Video", QString());
    if( !fileName.isNull() )
    {
        QImage temp(fileName);

        originalImage = temp.convertToFormat(QImage::Format_RGB32);

        ui->spnWidth->setValue(originalImage.width());
        ui->spnHeight->setValue(originalImage.height());

        resampler.setQImage(originalImage);
        qDebug()<<"setQImage";
        rescale();
        qDebug()<<"resacle";
        isLoaded = true;
        this->update();
        qDebug()<<"update";
    }
}

void MainWindow::rescale()
{
    if( (ui->spnWidth->value()>0) && (ui->spnHeight->value()>0) )
    {
        resampler.reScale(ui->spnWidth->value(), ui->spnHeight->value());
    }

    if( 0 )//(ui->spnWidth->value()>0) && (ui->spnHeight->value()>0) )
     {
        //rescaledImage = QImage(QSize(ui->spnWidth->value(), ui->spnHeight->value()), originalImage.format());
        QImage temp(originalImage.size(), originalImage.format());

        QRgb originalPixel;
        QRgb tempPixel;

        int *energy = new int[originalImage.width()*originalImage.height()];
        int *sum = new int[originalImage.width()*originalImage.height()];
        int *minimal = new int[originalImage.height()];

        int count = 0;
        int r, g, b;

        for (int i = 0; i < originalImage.height(); i++)
        {
            for (int j = 0; j < originalImage.width(); j++)
            {
                count = 0;
                r = 0;
                g = 0;
                b = 0;

                originalPixel = originalImage.pixel(j, i);

                if (i != originalImage.height() - 1)
                {
                    count++;
                    tempPixel = originalImage.pixel(j, i+1);

                    r += abs (qRed(originalPixel)   - qRed(tempPixel) );
                    g += abs (qGreen(originalPixel) - qGreen(tempPixel) );
                    b += abs (qBlue(originalPixel)  - qBlue(tempPixel) );
                }

                  // Если пиксел не крайний справа, то добавляем в sum разность между поточным пикселом и соседом справа
                if (j != originalImage.width() - 1)
                {
                    count++;
                    tempPixel = originalImage.pixel(j+1, i);

                    r += abs (qRed(originalPixel)   - qRed(tempPixel) );
                    g += abs (qGreen(originalPixel) - qGreen(tempPixel) );
                    b += abs (qBlue(originalPixel)  - qBlue(tempPixel) );
                }

                  // В массив energy добавляем среднее арифметическое разностей пикселя с соседями по k-той компоненте (то есть по R, G или B)
                if (count != 0)
                {
                    r /= count;
                    g /= count;
                    b /= count;
                    tempPixel = qRgba(r, g, b, 255);
                }
                else
                {
                    tempPixel = originalPixel;
                }

                energy[i*originalImage.width()+j] = qRed(tempPixel)+qGreen(tempPixel)+qBlue(tempPixel);
                temp.setPixel(j, i, tempPixel);
            }
        }

        //массив сумм находим
        for (int j = 0; j < originalImage.width(); j++)
             sum[j] = energy[j];

             // Для всех остальных пикселей значение элемента (i,j) массива sum будут равны
             //  energy[i,j] + MIN ( sum[i-1, j-1], sum[i-1, j], sum[i-1, j+1])
        for (int i = 1; i < originalImage.height(); i++)
        {
            for (int j = 0; j < originalImage.width(); j++)
            {
                sum[i*originalImage.width() + j] = sum[(i-1)*originalImage.width() + j];

                if ( (j > 0) && (sum[(i-1)*originalImage.width() + j-1] < sum[i*originalImage.width() + j]))
                    sum[i*originalImage.width() + j] = sum[(i-1)*originalImage.width() + j-1];

                if ((j < originalImage.width() - 1) && sum[(i-1)*originalImage.width() + j+1] < sum[i*originalImage.width() + j])
                            sum[i*originalImage.width() + j] = sum[(i-1)*originalImage.width() + j+1];

                sum[i*originalImage.width() + j] += energy[i*originalImage.width() + j];
            }
        }



        //---------
            int last = originalImage.height() - 1;
            // Выделяем память под массив результатов

            // Ищем минимальный элемент массива sum, который находиться в нижней строке и записываем результат в res[last]
            minimal[last] = 0;

            for (int j = 1; j < originalImage.width(); j++)
                if (sum[(originalImage.height() - 1)*originalImage.width() + j] < sum[(originalImage.height() - 1)*originalImage.width() + minimal[last]])
                    minimal[last] = j;

            // Теперь вычисляем все элементы массива от предпоследнего до первого.
            for (int i = last - 1; i >= 0; i--)
            {
                // prev - номер пикселя цепочки из предыдущей строки
                // В этой строке пикселями цепочки могут быть только (prev-1), prev или (prev+1), поскольку цепочка должна быть связанной
                int prev = minimal[i + 1];

                // Здесь мы ищем, в каком элементе массива sum, из тех, которые мы можем удалить, записано минимальное значение и присваиваем результат переменной res[i]
                minimal[i] = prev;

                if ( (prev > 0) && ( sum[(i)*originalImage.width() + minimal[i]] > sum[(i)*originalImage.width() + prev - 1]) )
                    minimal[i] = prev - 1;

                if ( (prev < originalImage.width() - 1) && (sum[(i)*originalImage.width() + minimal[i]] > sum[(i)*originalImage.width() + prev + 1]) )
                    minimal[i] = prev + 1;
            }

            rescaledImage = QImage(originalImage.width()+1, originalImage.height(), originalImage.format());//originalImage.copy(0, 0, originalImage.width()-1, originalImage.height());

            for (int i = 0; i < originalImage.height(); i++)
            {
                for(int j = 0; j< minimal[i] ; ++j)
                {
                    rescaledImage.setPixel(j, i, originalImage.pixel(j, i));//qRgba(255, 0, 0, 255));
                }

                r = 0;
                g = 0;
                b = 0;

                rescaledImage.setPixel(minimal[i], i, originalImage.pixel(minimal[i]+1, i));

                for(int j = minimal[i]+1; j<originalImage.width()+1; ++j)
                {
                    rescaledImage.setPixel(j, i, originalImage.pixel(j-1, i));//qRgba(255, 0, 0, 255));
                }
            }

        //---------

        delete []sum;
        delete []energy;
        delete []minimal;
        originalImage = rescaledImage.copy();
        this->update();
    }
}

bool MainWindow::eventFilter(QObject *obj, QEvent *ev)
{
    bool result = false;
    if(obj == ui->lblCanvas)
    {
        switch(ev->type())
        {
            case QEvent::Paint:
                this->drawFrame();
                result = true;
            break;
        }
    }
    return result;
}
//--------------------------

void MainWindow::on_actionLoad_image_triggered()
{
    loadImage();
}

void MainWindow::on_spnWidth_editingFinished()
{
    rescale();
    this->update();
}

void MainWindow::on_spnHeight_editingFinished()
{
    rescale();
    this->update();
}
