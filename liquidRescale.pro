#-------------------------------------------------
#
# Project created by QtCreator 2012-12-01T20:06:00
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = liquidRecale
TEMPLATE = app


SOURCES += main.cpp\
        mainWindow.cpp \
        rdLiquidRescale.cpp

HEADERS  += mainWindow.h \
            rdLiquidRescale.h

FORMS    += mainWindow.ui
