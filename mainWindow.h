#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFile>
#include <QFileDialog>
#include <QPainter>
#include <math.h>

#include "rdLiquidRescale.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
private:
    rdLiquidRescale resampler;
    QImage originalImage;
    QImage rescaledImage;
    bool isLoaded;

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void drawFrame();
    void loadImage();
    void rescale();
private slots:
    void on_actionLoad_image_triggered();

    void on_spnWidth_editingFinished();

    void on_spnHeight_editingFinished();

private:
    Ui::MainWindow *ui;
    virtual bool eventFilter(QObject *obj, QEvent *ev);
};

#endif // MAINWINDOW_H
