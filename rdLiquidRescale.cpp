#include <QDebug>

#include "rdLiquidRescale.h"

class rdImage
{
public:
    int *data;
    int width;
    int height;
    int channels;
public:
    rdImage()
    {
        data = 0;
        channels = 3;
    }
    ~rdImage()
    {
        delete []data;
    }
public:
    bool init(int w, int h, int channels_ = 3)
    {
        this->clearData();
        if( (w > 0) && (h>0) )
        {
            width = w;
            height = h;
            channels = channels_;
            data = new int[channels*width*height];
        }
        return false;
    }

    bool fromQImage(QImage &image)
    {
        if( (image.width() > 0) && (image.height() > 0) )
        {
            this->clearData();

            width  = image.width();
            height = image.height();

            data = new int[channels*width*height];

            int pos = 0;
            QRgb originalPixel;

            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    originalPixel = image.pixel(j, i);

                    data[pos    ] = qRed(originalPixel);
                    data[pos + 1] = qGreen(originalPixel);
                    data[pos + 2] = qBlue(originalPixel);
                    pos += 3;
                }
            }


            return true;
        }
        return false;
    }

    bool fromRDImage(rdImage &image)
    {
        if( (image.width > 0) && (image.height > 0) )
        {
            this->clearData();

            width  = image.width;
            height = image.height;

            channels = image.channels;

            data = new int[channels*width*height];

            int size = channels*width*height;

            for (int i = 0; i < size; i++)
            {
                data[i] = image.data[i];
            }

            return true;
        }
        return false;
    }

    bool toQImage(QImage &image)
    {
        if( (data) && (width > 0) && (height > 0) )
        {
            int pos = 0;
            QRgb originalPixel;

            image = QImage(width, height, QImage::QImage::Format_RGB32);

            if( channels == 3)
            {
                for (int i = 0; i < height; i++)
                {
                    for (int j = 0; j < width; j++)
                    {
                        originalPixel = qRgba(data[pos], data[pos + 1], data[pos + 2], 255);
                        image.setPixel(j, i, originalPixel);
                        pos += 3;
                    }
                }
            }
            else
            {
                if( channels == 1 )
                {
                    for (int i = 0; i < height; i++)
                    {
                        for (int j = 0; j < width; j++)
                        {
                            originalPixel = qRgba(data[pos], data[pos], data[pos], 255);
                            image.setPixel(j, i, originalPixel);
                            pos ++;
                        }
                    }
                }
            }
            return true;
        }
        return false;
    }

    bool clearData()
    {
        width  = 0;
        height = 0;

        channels = 3;

        delete []data;
        data = 0;
        return true;
    }

    int *getPixel(int x, int y)
    {
        return data+channels*(y*width+x);
    }

    int getPixelEnergy(int x, int y)
    {
        int sum =0;
        for(int i = 0; i < channels; i++)
        {
            sum += data[channels*(y*width+x) + i];
        }
        return (sum / channels);
    }

    void setPixel(int x, int y, int g)
    {
        for(int i = 0; i < channels; i++)
        {
            data[(y*width + x) + i] = g;
        }
    }

    void setPixel(int x, int y, int r, int g, int b)
    {
        if( channels == 3 )
        {
            data[channels*(y*width + x)    ] = r;
            data[channels*(y*width + x) + 1] = g;
            data[channels*(y*width + x) + 2] = b;
        }
    }


};

//--------------------------------------------//

class rdLiquidRescalePrivate
{
public:
    rdImage originalImage;
    rdImage rescaledImage;
    rdImage gradientImage;

    rdImage energyH;
    rdImage energyV;

    rdImage seamH;
    rdImage seamV;

public:
    rdLiquidRescalePrivate()
    {

    }
    ~rdLiquidRescalePrivate()
    {

    }
};

//--------------------------------------------//

rdLiquidRescale::rdLiquidRescale(QObject *parent) :
    QObject(parent)
{
    data = new rdLiquidRescalePrivate;
}

rdLiquidRescale::~rdLiquidRescale()
{
    delete data;
}

//--------------------------------------------//

bool rdLiquidRescale::setQImage(QImage &image)
{
    if (this->clearData())
    {
        if (data->originalImage.fromQImage(image))
        {
            if(this->calculateGradient())
            {
                if(this->calculateVerticalEnergy())
                {
                    if(this->calculateVerticalSeam())
                    {
                        return this->calculateHorizontalEnergy();
                    }
                }
            }

        }
    }
    return false;
}

bool rdLiquidRescale::getQImage(QImage &image)
{
    return data->originalImage.toQImage(image);
}

bool rdLiquidRescale::getScaledQImage(QImage &image)
{
    return data->rescaledImage.toQImage(image);
}

bool rdLiquidRescale::clearData()
{

    data->originalImage.clearData();
    data->rescaledImage.clearData();
    data->gradientImage.clearData();

    data->energyH.clearData();
    data->energyV.clearData();

    data->seamH.clearData();
    data->seamV.clearData();

    return true;
}

//--------------------------------------------//

bool rdLiquidRescale::reScale(int newWidth_, int newHeight_)
{

    if( (newWidth_ <= 0) || (newHeight_ <= 0) )
    {
        return false;
    }


    if( (newWidth_ == data->originalImage.width) && (newHeight_ == data->originalImage.height) )
    {

        //data->rescaledImage.fromRDImage(data->originalImage);
        data->rescaledImage.fromRDImage(data->gradientImage);
        //data->rescaledImage.fromRDImage(data->energyV);
        return true;
    }

    //data->rescaledImage.init(newWidth_, newHeight_);



    //data->rescaledImage.fromRDImage(data->gradientImage);
    return false;
}

bool rdLiquidRescale::calculateGradient()
{

    data->gradientImage.init(data->originalImage.width, data->originalImage.height);

    int count;
    int r, g, b;

    int *originalPixel;
    int *tempPixel;

    for (int i = 0; i < data->originalImage.height; i++)
    {
        for (int j = 0; j < data->originalImage.width; j++)
        {
            count = 0;
            r = 0;
            g = 0;
            b = 0;

            originalPixel = data->originalImage.getPixel(j, i);

            if (i != data->originalImage.height - 1)
            {
                count++;
                tempPixel = data->originalImage.getPixel(j, i+1);

                r += abs (originalPixel[0] - tempPixel[0] );
                g += abs (originalPixel[1] - tempPixel[1] );
                b += abs (originalPixel[2] - tempPixel[2] );
            }

              // Если пиксел не крайний справа, то добавляем в sum разность между поточным пикселом и соседом справа
            if (j != data->originalImage.width - 1)
            {
                count++;
                tempPixel = data->originalImage.getPixel(j+1, i);

                r += abs (originalPixel[0] - tempPixel[0] );
                g += abs (originalPixel[1] - tempPixel[1] );
                b += abs (originalPixel[2] - tempPixel[2] );
            }

              // В массив energy добавляем среднее арифметическое разностей пикселя с соседями по k-той компоненте (то есть по R, G или B)
            if (count != 0)
            {
                r /= count;
                g /= count;
                b /= count;
            }

            data->gradientImage.setPixel(j, i, r, g, b);
        }
    }
    return true;
}

bool rdLiquidRescale::calculateVerticalEnergy()
{
    data->energyV.init(data->originalImage.width, data->originalImage.height, 1);


    for (int x = 0; x < data->originalImage.width; x++)
         data->energyV.setPixel(x, 0, data->gradientImage.getPixelEnergy(x, 0));


    int left, right, center;
    for (int y = 1; y < data->originalImage.height; y++)
    {
        for (int x = 0; x < data->originalImage.width; x++)
        {
            center = data->gradientImage.getPixelEnergy(x, y-1);
            left = center;
            right = center;

            if(x > 0)
                left = data->gradientImage.getPixelEnergy(x-1, y-1);

            if(x < data->gradientImage.width-1)
                right = data->gradientImage.getPixelEnergy(x+1, y-1);

            if(left < center)
                center = left;
            if(right < center)
                center = right;

            data->energyV.setPixel(x, y, (center + data->gradientImage.getPixelEnergy(x, y)));
        }
    }

    return true;
}

bool rdLiquidRescale::calculateVerticalSeam()
{
    data->seamV.init(data->originalImage.width, data->originalImage.height, 1);
    // Теперь вычисляем все элементы массива от предпоследнего до первого.


    int *temp = new int[data->originalImage.width];
    int minimum;
    int index;
    int last;

    for( int i = 0; i < data->originalImage.width; i++)
    {
        temp[i] = (data->energyV.getPixel(i, data->originalImage.height - 1)[0]);
    }

    for( int i = 0; i < data->originalImage.width; i++)
    {

        minimum = -1;
        index = i;

        for( int j = 0; j < data->originalImage.width; j++)
        {
            if( temp[j] != -1)
            {
                if( minimum == -1 )
                {
                    minimum = temp[j];
                    index   = j;
                }
                else
                {
                    if( temp[j] < minimum )
                    {
                        minimum = temp[j];
                        index   = j;
                    }
                }
            }
        }

        temp[index] = -1;

        data->seamV.setPixel(i, data->originalImage.height - 1, index);
        //qDebug()<<index<<" "<<data->seamV.getPixel(i, data->originalImage.height - 1)[0];
    }

    for(int x = 0; x < data->originalImage.width; x++)
    {
        last = data->seamV.getPixel(x, data->originalImage.height - 1)[0];


        for(int y = data->originalImage.height - 2; y >= 0; y--)
        {
            minimum = data->energyV.getPixel(last, y)[0];
            index = last;

            if( last > 0 )
                if( minimum > data->energyV.getPixel(last - 1, y)[0] )
                {
                    minimum = data->energyV.getPixel(last - 1, y)[0];
                    index = last - 1;
                }

            if( last < data->originalImage.width - 1 )
                if( minimum > data->energyV.getPixel(last + 1, y)[0] )
                {
                    minimum = data->energyV.getPixel(last + 1, y)[0];
                    index = last + 1;
                }

            last = index;
            data->seamV.setPixel(x, y, index);
        }

    }

    QString str;
    for(int y = 0; y < data->originalImage.height; ++y)
    {
        //str = " ";
        for(int x = 0; x < data->originalImage.width; ++x)
        {
            //str += QString().setNum(data->seamV.getPixel(y, x)[0]);
          //  str += " ";

            data->gradientImage.setPixel(data->seamV.getPixel(x, y)[0], y, 255, 255.0*(float(data->seamV.getPixel(x, y)[0])/float(data->originalImage.width)), 0);
        }
        //qDebug()<<str;
    }
    delete []temp;

    /*


            // Теперь вычисляем все элементы массива от предпоследнего до первого.
            for (int i = last - 1; i >= 0; i--)
            {
                // prev - номер пикселя цепочки из предыдущей строки
                // В этой строке пикселями цепочки могут быть только (prev-1), prev или (prev+1), поскольку цепочка должна быть связанной
                int prev = minimal[i + 1];

                // Здесь мы ищем, в каком элементе массива sum, из тех, которые мы можем удалить, записано минимальное значение и присваиваем результат переменной res[i]
                minimal[i] = prev;

                if ( (prev > 0) && ( sum[(i)*originalImage.width() + minimal[i]] > sum[(i)*originalImage.width() + prev - 1]) )
                    minimal[i] = prev - 1;

                if ( (prev < originalImage.width() - 1) && (sum[(i)*originalImage.width() + minimal[i]] > sum[(i)*originalImage.width() + prev + 1]) )
                    minimal[i] = prev + 1;
            }

*/
    return true;
}

bool rdLiquidRescale::calculateHorizontalEnergy()
{
    return true;
}

bool rdLiquidRescale::calculateHorizontalSeam()
{
    return true;
}

//--------------------------------------------//

