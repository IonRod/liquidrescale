#ifndef RDLIQUIDRESCALE_H
#define RDLIQUIDRESCALE_H

#include <QObject>
#include <QImage>
#include <math.h>

class rdLiquidRescalePrivate;
class rdLiquidRescale : public QObject
{
    Q_OBJECT
private:
    rdLiquidRescalePrivate *data;

public:
    explicit rdLiquidRescale(QObject *parent = 0);
    ~rdLiquidRescale();

public:
    bool setQImage(QImage &image);
    bool getQImage(QImage &image);
    bool getScaledQImage(QImage &image);
    bool clearData();

public:
    bool reScale(int newWidth_, int newHeight_);
    bool calculateGradient();

    bool calculateVerticalEnergy();
    bool calculateVerticalSeam();

    bool calculateHorizontalEnergy();
    bool calculateHorizontalSeam();

signals:
    
public slots:
    
};

#endif // RDLIQUIDRESCALE_H
